#include <stdio.h>
#include <stdlib.h>
#include "prog1_2.h"

STACK* MakeStack(int initialCapacity) {
    struct stack *newStack = (struct stack*) malloc( sizeof(struct stack) * 1);
    newStack->capacity = initialCapacity;
    newStack->size = 0;
    newStack->data = (int*)malloc(sizeof(int) * initialCapacity);
    return newStack;
}

void Push(struct stack *aStack,int data) {
    if(data == aStack->capacity) {
        Grow(aStack);
    }
    aStack->data[aStack->size] = data;
    aStack->size++;
}

int Pop(struct stack *aStack) {
    if(aStack->size == 0) {
        return -1;
    }
    else {
        aStack->size--;
        return aStack->data[aStack->size];
    }
}

void Grow(struct stack *aStack) {
    struct stack *tempStack = MakeStack(aStack->capacity); //This stack is temporarily used to transfer the current stack onto the larger one
    tempStack->data = aStack->data;
    aStack->data = (int*)malloc(sizeof(int) * aStack->capacity * 2);
    aStack->data = tempStack->data;
    aStack->capacity = aStack->capacity * 2;
}
