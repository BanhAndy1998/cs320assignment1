Name : Andy Banh
Email : Banhandyofficial1998@gmail.com
REDID : 820256990

Program Descriptions

Overview - Program 1 consists of many smaller programs using the C language. Only prog1_1.c is a standalone program while prog1_3.c relies on prog1_2.c which also relies on prog1_2.h to operate.

prog1_1.c - Program 1-1 prompts the user to input a name. The program will then print a greeting to the user using the inputted name 

prog1_2.h - Header file for prog1_2.c. Contains the  prototypes and typedefs for a stack machine but no actual code.

prog1_2.c - This program implements prog1_2.h. Program 1-2 contains a stack machine and has the functions to allow the user to add onto and remove data off of it. The machine also automatically allocates more memory and will expand its size if the user reaches the capacity of the stack.

prog1_3.c - Program 1-3 is a driver program for prog1_2.c. The program must be started with a single integer argument or else the program will print an error message. Should the program successfully start, the program will take as many 256 character inputs from the user as given in the argument above. The only functionalities are "push" and "pop". To push, the user should type "push" and an integer. This will add a value onto the stack. To pop, the user should only type "pop". This will remove the last pushed value and print it to the user. The program is coded to ignore whitespaces before, between, and after arguments. No error message is given upon a invalid input but the user may type in another input if they have not reached the integer argument given when the program was executed.
