#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "prog1_2.h"

int main(int argc, char* argv[]) {
    STACK* mainStack = MakeStack(256);                  
    char input[256];
    char comm[256];
    char value[256];

    printf("Assignment #1-3, Andy Banh, Banhandyofficial1998@gmail.com\n");

    if(argc != 2) { //Checks that exactly 1 argument was given through the command line
        printf("This program expects a single command line argument.");
        exit(EXIT_SUCCESS);
    }

    long l = strtol(argv[1], NULL, 10); //This is used to check if the command line input is correct
    int length = 0; //This is used to check if the command line input is correct

    if(argv[1][0] == '0') { //This checks if the input was the integer 0
        length++;
    }
    for (int i = 0; i < strlen(argv[1]); i++) { //This loop divides the input by ten to find the number of digits
        if (l == 0) {
            break;
        }
        l /= 10;
        length++;
     }

    if(length != strlen(argv[1])) { //Checks if any non numeral value were entered into the input since strtol ignores non numerical inputs
        printf("This program expects a single command line argument.");
        exit(EXIT_SUCCESS);
    }

    int numInputs = atoi(argv[1]); //Numbers of times the program will take inputs

    while(numInputs != 0) { //If all of the above checks were valid then begin the main program function
        int wrongInput = 0; //This value is changed if the input is not exactly one command and one integer
        int firstLength = 0; //This value holds the length of characters in the first argument ignoring whitespaces
        int totalLength = 0; //This value holds the length of characters in the second argument ignoring whitespaces

        numInputs--;
        printf("> ");
        for(int k = 0; k < 256; k++) { //Resetting the values everytime the program  wraps back around
            input[k] = ' ';
            comm[k] = '\0';
            value[k] = '\0';
        }

        scanf(" %[^\n]s", input); 
    
        for(int k = 0; k < 256; k++) { //This checks the length of the first input
            if(input[k] == ' ' && firstLength > 0) { //Break out of loop once the first input has been read
                input[k] == '\0';
                firstLength = k + 1; //The additional 1 is to hop over the null terminator added above
                break;
            }
            if(input[k] == ' ') {
                continue;
            }
            if(input[k] != ' ') {
                comm[firstLength] = input[k];
                firstLength++;
                continue;
            }
        }
        if(firstLength == 256) { //If the above loop finds a input that takes up all 256 chars in the input then wrongInput is set to true
            wrongInput = 1;
        }
        for(int k = firstLength; k < 256; k++) {
            if(totalLength == 0 && input[k] == '\0') { //Corrects the input incase additional whitespaces are entered
                input[k] = ' ';
            }
            if(totalLength != 0 && strcmp(comm, "pop") == 0) { //If pop was entered and any other inputs were added then wrongInput is set to true
                wrongInput = 1;
                break;
            }
            if(totalLength == 0 && k == 255 && input[k] == ' ' && strcmp(comm, "pop") == 0) { //See next for loop for explanation
                totalLength = 255; 
                break;
            } 
            if(totalLength == 0 && k == 255 && input[k] == ' ') { //If there was no integer argument then set wrongInput to true
                wrongInput = 1;
                break;
            }
            if(totalLength > 0 && input[k] == ' ') { //Once the second input has been read, exit the loop
                totalLength = k;
                break;
            }
            if(input[k] != ' ') {
                value[totalLength] = input[k];
                totalLength++;
                continue;
            }
        }
        for(int k = totalLength; k < 256; k++) { //Checks for third or more inputs. If pop was the sole input, 2nd if statement in above loop prevents this from setting wrongInput to true
            if(input[k] == '\0') { //Corrects the input incase additional whitespaces are entered
                input[k] = ' ';
            }
            if(input[k] != ' ') {
                wrongInput = 1;
                break;
            }
        }
        if(wrongInput == 1) { //If exactly one string and integer or only pop were not given then ignore the input and take the next input.
            continue;
        }
        else if(strcmp(comm, "pop") == 0) {
            printf("%d\n", Pop(mainStack));  
        }
        else if (strcmp(comm, "push") == 0) {
            Push(mainStack, atoi(value));
        }
    }
}
